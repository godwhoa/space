local function round_down(n,m)
	return m*(math.floor(n/m));
end

function nearest_topleft(x,y,ww,wh)
	return round_down(x,ww), round_down(y,wh)
end