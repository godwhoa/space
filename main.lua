Object = require 'libs.classic'
Camera = require 'libs.camera'
Stats  = require 'stats'
require 'starfield'
require 'player'


ww,wh = 1000,700
love.window.setMode(ww,wh,{msaa=8})
love.window.setTitle("Shoot")

function love.load()
	camera = Camera(ww,wh)
	starfield = Starfield()
	player = Player()
	stats = Stats()
end

function love.mousemoved(x, y)
	player:mousemoved(x,y)
end

function love.mousepressed(x, y, button)
	player:mousepressed(x,y,button)
end

function love.keypressed(key)
	stats:keypressed(key)
end

function love.update(dt)
	player:update(dt)
end

function love.draw()
	-- uses a canvas so calls camera:set once canvas is set.
	starfield:draw()

	camera:set()
	player:draw()
	camera:unset()

	stats:draw()
end