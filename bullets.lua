require 'libs.math'

Bullets = Object:extend()

function Bullets:new()
	-- physics
	self.speed = 800
	self.limit = 30
	-- container
	self.bullets = {}
end

function Bullets:add(x1,y1,x2,y2)
	if #self.bullets < self.limit then
		local angle = math.angle(x1,y1,x2,y2)
		table.insert(self.bullets,{
			start  = {x=x1,y=y1},
			finish = {x=x2,y=y2},
			x=x1,y=y1,
			angle=angle
		})
	end
end

function Bullets:remove(i)
	table.remove(self.bullets,i)
end

function Bullets:update(dt,cb)
	if love.keyboard.isDown("up") then
		self.speed = self.speed + 1
	end

	if love.keyboard.isDown("down") then
		self.speed = self.speed - 1
	end

	for i,b in ipairs(self.bullets) do
		if cb ~= nil then
			cb(i,b)
		end
		-- remove if out of bounds
		if b.x < camera.x or b.x > camera.bw then
			self:remove(i)
		end

		if b.y < camera.y or b.y > camera.bh then
			self:remove(i)
		end

		-- movement
		b.vx,b.vy = math.cos(b.angle),math.sin(b.angle)

		b.x = b.x + b.vx * dt * self.speed
		b.y = b.y + b.vy * dt * self.speed
	end
end

function Bullets:draw()
	for i,b in ipairs(self.bullets) do
		love.graphics.circle("fill", b.x,b.y, 5, 100)
	end
end