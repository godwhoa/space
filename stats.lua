Stats = Object:extend()

function Stats:new()
	self.font = love.graphics.newFont("assets/RobotoMono.ttf", 15)
	love.graphics.setFont(self.font)
	self.isdebug = false
end

function Stats:keypressed(key)
	if key == 'tab' then
		self.isdebug = not self.isdebug
	end
	if key == 'c' then
		collectgarbage("collect")
	end
end

function Stats:draw()
	if self.isdebug then
		local mem = collectgarbage('count')/1000
		local fps_stat    = ("FPS: %d Mem: %.2fMB"):format(love.timer.getFPS(),mem)
		local player_stat = ("player x:%d y:%d vx:%d vy:%d"):format(player.x,player.y,player.vx,player.vy)
		local camera_stat = ("camera x:%d y:%d"):format(camera.x,camera.y)
		love.graphics.printf(fps_stat, 20, 20, 250, 'left')
		love.graphics.printf(player_stat, 20, 40, 350, 'left')
		love.graphics.printf(camera_stat, 20, 60, 250, 'left')
	end
end

return Stats