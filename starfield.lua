require 'libs.utils'

Starfield = Object:extend()

function Starfield:new()
	-- off-screen star canvas
	self.canvas = love.graphics.newCanvas(ww, wh)
	self.canvas:setFilter("nearest","nearest",5)
	-- limit
	self.max_stars = 500
	-- container for star sections
	-- stars[x][y] = {x,y...}
	self.stars = {}
	-- init
	self:add_surround(20,20)
end

function Starfield:add_section(x,y,w,h)
	-- make multi-dimentional table possible
	if self.stars[x] == nil then
		self.stars[x] = {}
	end
	if self.stars[x][y] == nil then
		self.stars[x][y] = {}
	end

	-- prevent over-writting
	if #self.stars[x][y] == 0 then
		for i=1,self.max_stars do
			table.insert(self.stars[x][y],love.math.random(x,x+w))
			table.insert(self.stars[x][y],love.math.random(y,y+h))
		end
	end
end

function Starfield:add_surround(x,y)
	local surround = {}
	-- nearest point to player on the wwxwh grid
	local nx, ny = nearest_topleft(x,y,ww,wh)

	-- north
	self:add_section(nx,ny-wh,ww,wh)
	-- north west
	self:add_section(nx-ww,ny-wh,ww,wh)
	-- north west
	self:add_section(nx+ww,ny-wh,ww,wh)

	-- west
	self:add_section(nx-ww,ny,ww,wh)
	-- center
	self:add_section(nx,ny,ww,wh)
	-- east
	self:add_section(nx+ww,ny,ww,wh)

	-- south west
	self:add_section(nx-ww,ny+wh,ww,wh)
	-- south
	self:add_section(nx,ny+wh,ww,wh)
	-- south east
	self:add_section(nx+ww,ny+wh,ww,wh)
end

-- draw
function Starfield:draw()
	love.graphics.setCanvas(self.canvas)
	love.graphics.clear()
	camera:set()
	love.graphics.setColor(255,255,255)
	for i,depth1 in pairs(self.stars) do
		for j,depth2 in pairs(depth1) do
			love.graphics.points(depth2)
		end
	end
	camera:unset()
	love.graphics.setCanvas()
	love.graphics.draw(self.canvas)
end