require 'bullets'
require 'libs.math'

Player = Object:extend()


function Player:new()
	-- sprite
	self.img = love.graphics.newImage("assets/player.png")
	self.w,self.h = self.img:getWidth(),self.img:getHeight()
	-- moving around
	self.x,self.y = ww/2,wh/2
	self.vx,self.vy = 0,0
	self.displace = 8
	-- transforms
	self.sx,self.sy = 1,1
	self.ox,self.oy = self.w/2,self.h/2
	self.angle = 0
	-- input
	self.mx,self.my = 0,0
	-- bullets
	self.bullets = Bullets()
end

function Player:mousemoved(x,y)
	self.mx,self.my = x+camera.x,y+camera.y
	self.angle = math.atan2(self.my - self.y, self.mx - self.x)
end

function Player:mousepressed(x,y,button)
	self.mx,self.my = x+camera.x,y+camera.y
	self.angle = math.atan2(self.my - self.y, self.mx - self.x)
	
	if button == 1 then
		self.bullets:add(self.x,self.y,self.mx,self.my)
	end
end

function Player:movement()
	-- up
	if love.keyboard.isDown("w") then
		self.vy = self.vy - self.displace
	end

	-- left
	if love.keyboard.isDown("a") then
		self.vx = self.vx - self.displace
	end

	-- down
	if love.keyboard.isDown("s") then
		self.vy = self.vy + self.displace
	end

	-- right
	if love.keyboard.isDown("d") then
		self.vx = self.vx + self.displace
	end
end

function Player:update(dt)
	self:movement()
	self.bullets:update(dt)
	-- update bullet bounds
	self.bullets.ww,self.bullets.wh = camera.bw,camera.bh
	-- friction
	self.vx = self.vx * 0.99
	self.vy = self.vy * 0.99
	-- intergate
	self.x = self.x + self.vx * dt
	self.y = self.y + self.vy * dt
	-- move camera with player
	camera.x = self.x - ww/2
	camera.y = self.y - wh/2

	starfield:add_surround(self.x,self.y)

	-- print(string.format("camx: %d camy: %d",camera.x,camera.y))
	-- print(string.format("bw: %d bh: %d",camera.bw,camera.bh))
	-- print(string.format("mx: %d my: %d",self.mx,self.my))
end

function Player:draw()
	-- camera:set()
	self.bullets:draw()
	love.graphics.draw(
		self.img,
		self.x,
		self.y,
		self.angle,
		self.sx,
		self.sy,
		self.ox,
		self.oy
	)
	-- camera:unset()
end